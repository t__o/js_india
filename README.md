![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

Javascript Animation using Canvas

---

[Demo](https://t__o.gitlab.io/js_india)

[Indian Style](https://t__o.gitlab.io/js_india/india.html)

This is not responsive or mobile optimised, better on big screen.